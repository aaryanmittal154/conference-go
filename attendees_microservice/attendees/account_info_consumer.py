from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO # noqa


def update_account_vo(ch, method, properties, body): # runs when consumer receives the publish # noqa
    content = json.loads(body) # the body that was sent as 'body' from the producer # noqa
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated = datetime.fromisoformat(content["updated"])

    if is_active:
        AccountVO.objects.filter(updated__lt=updated).update_or_create(
            email=email,
            defaults={
                "updated": updated,
                "first_name": first_name,
                "last_name": last_name,
            },
        )
    else:
        AccountVO.objects.filter(email=email).delete()


while True:  # infinite loop
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        channel.exchange_declare(
            exchange="account_info",
            exchange_type="fanout",
        )

        result = channel.queue_declare(queue="", exclusive=True)
        queue_name = result.method.queue

        channel.queue_bind(exchange="account_info", queue=queue_name)

        channel.basic_consume(
            queue=queue_name,
            on_message_callback=update_account_vo,
            auto_ack=True,
        )

        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
