import json
import pika
import django
import os
import sys

from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_message(ch, method, properties, body):
    presentation = json.loads(body)
    emotion = 'happy' if presentation['status'] == 'approval' else 'sad'
    status = 'approved' if presentation['status'] == 'approval' else 'rejected'
    send_mail(f"Your presentation has been {status}",
                f"Dear {presentation['presenter_name']}, We\'re {emotion} to tell you your presentation ({presentation['title']}) has been {status}", # noqa
                'admin@conference.go',
                [presentation['presenter_email']])


parameters = pika.ConnectionParameters(host='rabbitmq')
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue='presentation_status')
channel.basic_consume(
    queue='presentation_status',
    on_message_callback=process_message,
    auto_ack=True,
)
channel.start_consuming()
